extern crate plain_ui;
extern crate piston;
extern crate texture;
extern crate graphics;
extern crate gl;
extern crate opengl_graphics;

pub mod renderer;
pub mod backend;

#[cfg(test)]
mod tests;

use plain_ui::*;

use opengl_graphics::*;
use graphics::*;

pub use renderer::*;

pub fn img_button_min_size(t: &mut PistonTheme, down: bool) -> [f32; 2] {
    t.rect_min_size((1, if down { 1 } else { 0 }))
}

pub fn img_button(
    r: &mut PistonRenderer,
    t: &mut PistonTheme,
    down: bool,
    rect: Rect,
    img: &Texture
) {
    let style = (1, if down { 1 } else { 0 });
    let border = t.border_size(style);

    r.rect(t, rect, style);

    let ratio = img.get_width() as f64 / img.get_height() as f64;

    let (width, height) = if img.get_width() > img.get_height() {
        (0f64.max((rect.1[0] - border * 2.0) as f64),
         0f64.max((rect.1[1] - border * 2.0) as f64 / ratio))
    } else {
        (0f64.max((rect.1[0] - border * 2.0) as f64 * ratio),
         0f64.max((rect.1[1] - border * 2.0) as f64))
    };

    let image = Image::new().rect([
        (0f64.max((rect.1[0] - border * 2.0) as f64) - width) / 2.0,
        (0f64.max((rect.1[1] - border * 2.0) as f64) - height) / 2.0,
        width,
        height
    ]);

    image.draw(
        img,
        &r.c.draw_state,
        r.c.transform.trans((rect.0[0] + border) as f64, (rect.0[1] + border) as f64),
        r.g
    );
}
